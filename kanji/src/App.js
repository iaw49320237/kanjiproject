import React, {useState, useEffect } from 'react';
import "./css/App.css";
import Login from './components/Login';
import Register from './components/Register';
import Main from './components/Main';
import CustomList from './components/CustomList';
import { BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";

import ClipLoader from "react-spinners/ClipLoader"
import {
  toAscii,
  toFullwidth,
  toHiragana,
  toKatakana,
  toFullwidthKana,
  toHalfwidthKana,
  toNfc,
  toNumeric,
  toNumericFromKanji,
  addCommas,
  normalizeHyphens,
} from 'japanese-string-utils';
import * as wanakana from 'wanakana';

function App() {


  
    return (
      <div id="app">
      <Router>
        <Switch>
          <Route path="/login"> <Login /> </Route>
          <Route path="/register"> <Register /> </Route>
          <Route path="/yourkanjilist"> <CustomList /> </Route>
          <Route path="/"> <Main /> </Route>
          
          
        </Switch>
     
      </Router>
     </div>
    );
  
}

export default App;