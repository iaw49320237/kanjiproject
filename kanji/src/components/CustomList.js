import React, {useState, useEffect } from 'react';
import "./../css/Main.css";
import jwt_decode from "jwt-decode";
import { BrowserRouter as Router, Route, Switch, Link, useHistory} from 'react-router-dom'
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import ClipLoader from "react-spinners/ClipLoader"
import {
  toAscii,
  toFullwidth,
  toHiragana,
  toKatakana,
  toFullwidthKana,
  toHalfwidthKana,
  toNfc,
  toNumeric,
  toNumericFromKanji,
  addCommas,
  normalizeHyphens,
} from 'japanese-string-utils';
import * as wanakana from 'wanakana';

function CustomList() {
  let history = useHistory();
 const [loading, setLoading] = useState(false)
 const [kanjis, setKanjis] = useState([])
 const [fullKanjiList, setfullKanjiList] = useState([])
 const [jlptFilter, setjlptFilter] = useState([])
 const [kanjiList, setKanjiList] = useState([])
 const [searchTerm, setSearchTerm] = useState('')
 const [userName, setUserName] = useState("")
 const [visible, setVisible] = useState(20)
 const [filtre, setFiltre] = useState('')
 const [customKanjiList, setCustomKanjiList] = useState([])
 const [isAuth, setIsAuth] = useState(false)
 
 
  /*
  refresh() {
  localStorage.setItem("page", String(this.state.page + 10))
   window.location.reload(false);
  
 
  }
  
*/


function redirectToMainPage() {
  if(kanjiList.length != 0) {
    history.push({
      pathname: '/',
      passedKanjiList: kanjiList,
    });
  } else {

  }
      history.push({
          pathname: '/',
          
        });
  
}

function logOut(){
  localStorage.removeItem("jwt");
  
  history.push({
    pathname: '/login',
    passedKanjiList: kanjiList,
  });
}





 const showMoreKanjis = () => {
   setVisible((prevValue) => prevValue + 10);
   console.log(customKanjiList);
 }

 function toggleCheckbox(e) {
   
  console.log("checkeat")
  
  if(e.target.checked){
    
   if(jlptFilter === []) {
    setjlptFilter(jlptFilter => [...jlptFilter, e.target.value])
    console.log(jlptFilter)
   } else {
    console.log("checkeat")
    
    const array = jlptFilter.slice(0);
    
    array.push(e.target.value); 
    
    setjlptFilter(array);
    console.log(jlptFilter)
   }
    
    
  }

  if(!e.target.checked){
    console.log("descheckeat")
    const newArr = jlptFilter.filter(element => element !== e.target.value)
    setjlptFilter(newArr);
    console.log(jlptFilter)
    

  }
}

function meaningsFilter(kanji) {
  var found = false;
 if (kanji.meanings.length !== 0){
    
    kanji.meanings.forEach(element => {
      if( element.toLowerCase().includes(searchTerm.toLocaleLowerCase())){
        found = true;
      }
    
    });
    return found
}
}

function kun_readingsFilter(kanji) {
 
  var found = false;
 if (kanji.kun_readings.length !== 0){
  
    kanji.kun_readings.forEach(element => {
      var filtered = (element.replace(".",""))
      var filteredSearchTerm = wanakana.toHiragana(searchTerm);
      if( filtered.includes(filteredSearchTerm)){
        found = true;
      }
    
    });
    return found
}
}

function on_readingsFilter(kanji) {
 
  var found = false;
 if (kanji.on_readings.length !== 0){
    
    kanji.on_readings.forEach(element => {
      var filtered =  element.replace(".","")
      
      var filteredSearchTerm = wanakana.toKatakana(searchTerm);
      
      if( filtered.includes( filteredSearchTerm)){
        found = true;
      }
    
    });
    return found
}
}

function name_readingsFilter(kanji) {
  var found = false;
 if (kanji.name_readings.length !== 0){
    
    kanji.name_readings.forEach(element => {
      var filtered = wanakana.toHiragana(element.replace(".",""))
      var filteredSearchTerm = wanakana.toHiragana(searchTerm);
      if( filtered.includes(filteredSearchTerm)){
        found = true;
      }
    
    });
    return found
}
}
function getCustomKanjiList(){
  let token = localStorage.getItem("jwt");
  var decode = jwt_decode(token);
  let id = decode.id
 
  
  axios.get("http://localhost:5000/api/users/customlist/" + id).then((response) => {
        
        console.log(response.data.customKanjiList)
        
        setCustomKanjiList(response.data.customKanjiList)
        
      });

}
function removeKanji(kanji){
  let token = localStorage.getItem("jwt");
  var decode = jwt_decode(token);
  let id = decode.id
  var character = {kanji: kanji}
  
  axios.post("http://localhost:5000/api/users/removekanji/" + id, character).then((response) => {
        
        console.log(response.data)
        
        
  });

  document.getElementById("id"+kanji).style.display = "none";
}
 useEffect(async () => {
   console.log(history.location.passedKanjiList)
  if(history.location.passedKanjiList) {
    setKanjiList(history.location.passedKanjiList);
  }
 
  setjlptFilter([]);

      setLoading(true)

      console.log("hola")
      let token = localStorage.getItem("jwt");
      var decode = jwt_decode(token);
      let id = decode.id
      setUserName(decode.name)
  
    await axios.get("http://localhost:5000/api/users/customlist/" + id).then((response) => {
        
          const kanjis = response.data.customKanjiList;
          setCustomKanjiList(response.data.customKanjiList)
          console.log(kanjis)


          const promises = kanjis.map(async kanji => {
            return await axios.get("https://kanjiapi.dev/v1/kanji/"+kanji)
            .then(res=> {return res.data})
          })
          Promise.all(promises).then( data =>  {
          console.log(data)
            setfullKanjiList(data)
            
          console.log("holaa")
          setLoading(false)
          });
      
      
   });

  
  
  
  
 },[]);


  
    return (
      <div id="app">
      { loading ?   <div id="loader"> <div id="load-text"> <h2 >Loading Your Custom Kanji List...<br></br> <div> <br></br>お待ちください</div> </h2> </div> <div id="spinner" > <ClipLoader  size={120} color={'white'} loading={loading}> </ClipLoader> </div>  </div> 
                    
      :
      
        
        <div>
          <nav id="navbarkanji" class="navbar">
           
               <span class="navbar navbar-left"> <img src="logo.png" width="44" height="40" class="d-inline-block align-top" alt=""/>   </span>
              
               <div class="navbar navbar-right" id="custombtn" ><button onClick={redirectToMainPage} type="button" class="btn btn-link ">Back To Main Page</button> <a onClick={logOut} class="nav-link">Log out</a></div>
          </nav>
        <div class="row text-center text-white mb-2 mt-4">
          <div class="col-lg mx-auto">
            <h1 class="display-4">{userName}'s Custom Kanji List</h1>
          </div>
        </div>
        <div class="container py-5">
          <div class="input-group">
            <input
              type="search"
              class="form-control rounded"
              placeholder="Search"
              aria-label="Search"
              aria-describedby="search-addon"
              onChange={event => {setSearchTerm(event.target.value)}}
            />
           
          </div>
          {/*
            <div class="row text-center text-white mb-5">
              <div class="col-lg-7 mx-auto">
                <h1 class="display-4">Bootstrap 4 list</h1>
                <p class="lead mb-0">Create an elegant product list using Bootstrap 4 list group</p>
                <p class="lead">Snippet by <a href="https://bootstrapious.com/snippets" class="text-white"> 
                          <u>Bootstrapious</u></a>
                </p>
              </div>
            </div>
          */}

          <div class="row" id="contenidor">
            <div class="col-sm-10" id="list">
              
              <ul class="list-group shadow">
                {fullKanjiList.filter((kanji) => {
                  
                  if(searchTerm === ""){
                     return kanji 
                  } else if (kanji.heisig_en != null && kanji.heisig_en.toLowerCase().includes(searchTerm.toLocaleLowerCase())){
                    return kanji 
                  } else if (meaningsFilter(kanji)){
                   return kanji
                  } else if (kun_readingsFilter(kanji)){
                   return kanji
                  } else if (on_readingsFilter(kanji)){
                    return kanji
                  } else if (name_readingsFilter(kanji)){
                    return kanji
                     
                  } else if (kanji.kanji != null && kanji.kanji == searchTerm){
                    return kanji 
                  } 
              }).filter((kanji) => {
                  
                if(jlptFilter.length == 0){
                  return kanji 
                }  else if (kanji.jlpt != null){
                var found = false;
                jlptFilter.forEach(element => {
                  if(element == kanji.jlpt) {
                    found = true;
                  }
                });
                if(found) {
                  return kanji
                }
              
              } 
              }).slice(0,visible).map((kanji) => (
                  <li id={"id"+kanji.kanji} class="list-group-item">
                    <div
                      class="container-fluid d-flex h-100 flex-column"
                      id="kanji"
                    >
                      <div class="row flex-grow-1">
                        <div class="col-2">
                          <div id="caracter"> {kanji.kanji}</div>
                        </div>
                        <div class="col-10">
                            <div class="row">
                              <div class="col-7">
                              {" "}
                                  <div id="lecturas">
                                    <span id="titles"><strong>Kun: </strong></span>  {kanji.kun_readings.map(reading => {
                                          if(reading != kanji.kun_readings[kanji.kun_readings.length -1]){
                                            return reading + " / ";

                                          } else {
                                            return reading
                                          }})} <br /> <span id="titles"><strong>On: </strong></span>  {kanji.on_readings.map(reading => {
                                            if(reading != kanji.on_readings[kanji.on_readings.length -1]){
                                              return reading + " / ";
  
                                            } else {
                                              return reading
                                            }})} <br />
                                    
                                  </div>
                              </div>
                              <div class="col-4">
                              <span id="titles"><strong>Strokes: </strong></span>     {kanji.stroke_count}, <br /> <span id="titles"><strong> JLPT: </strong></span> {" "}
                                  {kanji.jlpt} <br />
                                
                              </div>
                              <div class="col-1">
                             
                              <div id={kanji.kanji}  onClick={() => { removeKanji(kanji.kanji) }}><button type="button" class="btn btn-danger "><i class="fa fa-times" aria-hidden="true"></i></button></div> 
                            
                              </div>
                              
                          </div>
                          <div class="row">
                            <div class="col-12" id="meanings">
                            <span id="titles"><strong>Meanings: </strong></span> {kanji.meanings.map(meaning => {
                                    if(meaning != kanji.meanings[kanji.meanings.length -1]){
                                      return meaning + ";    ";

                                    } else {
                                      return meaning
                            }})} 
                            </div>
                            
                          </div>
                        
                        </div>
                      </div>
                      
                      
                    
                    </div>
                  </li>
                ))}
              </ul>
            </div>
            <div class="col-md-2 " id="filtros">
                    

            <article class="card-group-item">
            <header class="card-header">
              <h6 class="title">JLPT (Former JLPT Ranking)</h6>
            </header>
            <div class="filter-content">
              <div class="card-body">
              <form>
               
                <label class="form-check">
                  <input onChange={toggleCheckbox} class="form-check-input" type="checkbox" value="4"/>
                  <span class="form-check-label">
                  Noken 4
                  </span>
                </label>  
                <label class="form-check">
                  <input onChange={toggleCheckbox} class="form-check-input" type="checkbox" value="3"/>
                  <span class="form-check-label">
                  Noken 3
                  </span>
                </label> 
                <label class="form-check">
                  <input  onChange={toggleCheckbox} class="form-check-input" type="checkbox" value="2"/>
                  <span class="form-check-label">
                  Noken 2
                  </span>
                </label> 
                <label  class="form-check">
                  <input onChange={toggleCheckbox} class="form-check-input" type="checkbox" value="1"/>
                  <span class="form-check-label">
                  Noken 1
                  </span>
                </label> 
              </form>

              </div> 
            </div>
          </article>
           </div  >
          </div>
          
        </div>
        {}
        <div id="loadmore" class="row justify-content-center">
        <button  class="btn btn-dark" onClick={showMoreKanjis}>Load more Kanji</button>
        </div>
       
        
        <ul></ul>
      </div>
      }
      </div>
    );
  
}

export default CustomList;