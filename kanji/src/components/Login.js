import React, {useState, useEffect } from 'react';
import "./../css/Login.css";
import { BrowserRouter as Router, Route, Redirect,  useHistory} from 'react-router-dom';

import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
function Login() {
 
    let history = useHistory();
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    
    const [errors, setErrors] = useState("");
    const [kanjiList, setKanjiList] = useState([])
    
    useEffect(async () => {
        console.log(history.location.passedKanjiList)
        if(history.location.passedKanjiList) {
            setKanjiList(history.location.passedKanjiList);
        }
 
    },[]);
      

    function redirectToMain() {
        if(kanjiList.length != 0){
            history.push({
                pathname: '/',
                passedKanjiList: kanjiList,
              });
        } else {
            history.push('/');  
        }
        
    }
    
    function redirectToRegister() {
  
        history.push({
          pathname: '/register',
          passedKanjiList: kanjiList,
        });
      
        
      }

    function onChange(e)  {
     if(e.target.id == "email"){
        setEmail(e.target.value);
    } else if (e.target.id == "password"){
        setPassword(e.target.value);
    }
     };
     function onSubmit(e)  {
   
        e.preventDefault();
    const user = {
          email: email,
          password: password,
         
    };
    
    axios
        .post("http://localhost:5000/api/users/login", user)
        .then((response) => {
            var token = response.data.token
            localStorage.setItem("jwt", token);
            console.log(token)
            history.push({
                pathname: '/',
                passedKanjiList: kanjiList,
              });
        })
        .catch((error) => {
            setErrors(error.response.data[0])
            console.log(error)
            console.log(error.data)
        })

  };
  
    return (
      <div id="login">
          
<div class="container login-container">
            <div class="row justify-content-center">
                <div class="col-md-6 login-form-2">
                    <h3>Login</h3>
                   
                    <form  onSubmit={onSubmit}>
                    {errors !== "" &&
                    <div class="alert alert-danger" role="alert">
                    {errors}                 
                    </div>
                    }
                        <div class="form-group">
                            <input type="text" id="email" onChange={onChange} class="form-control" placeholder="Your Email" value={email} required/>
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" onChange={onChange} class="form-control" placeholder="Your Password" value={password} required/>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Login" />
                        </div>
                        
                    </form>
                    <a onClick={redirectToRegister}  class="link">Register</a>
                </div>
                <div id="close"  onClick={redirectToMain}><button type="button" class="btn btn-dark ">X</button></div> 

            </div>
        </div>
      </div>
    );
  
}

export default Login;