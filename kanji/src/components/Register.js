import React, {useState, useEffect } from 'react';
import "./../css/Register.css";
import { BrowserRouter as Router, Route, Redirect,  useHistory} from 'react-router-dom';

import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
function Register() {
    let history = useHistory();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");
    const [errors, setErrors] = useState("");
    const [kanjiList, setKanjiList] = useState([])
    
    useEffect(async () => {
        console.log(history.location.passedKanjiList)
        if(history.location.passedKanjiList) {
            setKanjiList(history.location.passedKanjiList);
        }
 
    },[]);
      

    function redirectToMain() {
        if(kanjiList.length != 0){
            history.push({
                pathname: '/',
                passedKanjiList: kanjiList,
              });
        } else {
            history.push('/');  
        }
        
    }

    function redirectToLogin() {
  
        history.push({
          pathname: '/login',
          passedKanjiList: kanjiList,
        });
      
        
      }

    function onChange(e)  {
     if(e.target.id == "name"){
         setName(e.target.value);
     } else if (e.target.id == "email"){
        setEmail(e.target.value);
    } else if (e.target.id == "password"){
        setPassword(e.target.value);
    } else if (e.target.id == "password2"){
        setPassword2(e.target.value);
    }
     };
  


  function onSubmit(e)  {
   
        e.preventDefault();
    const newUser = {
          name: name,
          email: email,
          password: password,
          password2: password2
    };
    
    axios
        .post("http://localhost:5000/api/users/register", newUser)
        .then((response) => {
            
            console.log(response)
            history.push({
                pathname: '/login',
                passedKanjiList: kanjiList,
              });
        })
        .catch((error) => {
            
            setErrors(error.response.data[0])
            console.log(error.response.data)
            console.log(error.message)
        })

  };
  
    return (
      <div id="register">
     
<div class="container login-container">
       
            <div class="row justify-content-center">
           
                <div class="col-md-6 login-form-2">
               
                    <h3>Sign Up</h3>
                   
                                    
                    
                    
                    <form  onSubmit={onSubmit}>
                    {errors !== "" &&
                    <div class="alert alert-danger" role="alert">
                    {errors}                 
                    </div>
                    }
                         <div class="form-group">
                            <input type="text" id="name" onChange={onChange} class="form-control" placeholder="Your Name" value={name} required/>
                        </div>
                        <div class="form-group">
                            <input type="text" id="email" onChange={onChange}class="form-control" placeholder="Your Email" value={email} required/>
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" onChange={onChange} class="form-control" placeholder="Your Password" value={password} required/>
                        </div>
                        <div class="form-group">
                            <input type="password" id="password2"onChange={onChange} class="form-control" placeholder="Repeat your Password" value={password2} required/>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="register" />
                        </div>
                        
                    </form>
                    <a onClick={redirectToLogin} class="nav-link">Log In</a>
                </div>
                <div id="close" onClick={redirectToMain}><button type="button" class="btn btn-dark ">X</button></div> 
            </div>
            
        </div>
      </div>
    );
  
}

export default Register;