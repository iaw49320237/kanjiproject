const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
// Load User model
const User = require("../../models/User");

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
    // Form validation
  const { errors, isValid } = validateRegisterInput(req.body);
  // Check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
  User.findOne({ email: req.body.email }).then(user => {
      if (user) {
        return res.status(400).json(["Email already exists"]);
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        });
  // Hash password before saving in database
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err));
          });
        });
      }
    });
  });

  // @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
    // Form validation
  const { errors, isValid } = validateLoginInput(req.body);
  // Check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
  const email = req.body.email;
    const password = req.body.password;
  // Find user by email
    User.findOne({ email }).then(user => {
      // Check if user exists
      if (!user) {
        return res.status(404).json(["Email not found"]);
      }
  // Check password
      bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          // User matched
          // Create JWT Payload
          const payload = {
            id: user.id,
            name: user.name
          };
  // Sign token
          jwt.sign(
            payload,
            keys.secretOrKey,
            {
              expiresIn: 31556926 // 1 year in seconds
            },
            (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token
              });
            }
          );
        } else {
          return res
            .status(400)
            .json(["Password incorrect" ]);
        }
      });
    });
  });

  router.get("/customlist/:id", async (req, res) => {
    var id = req.params.id;
    var customlist =  await User.findOne({ _id: id }).select({customKanjiList: 1});
    console.log(customlist)
    return res.status(200).jsonp(customlist);
  });

  router.post("/addkanji/:id", (req, res) => {
    var id = req.params.id;
    var kanji = req.body.kanji;
    User.findOneAndUpdate(
      { _id: id }, 
      { $push: { customKanjiList: kanji } },
      function (error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log(success);
        }
     
      });
      return res.status(200).send('Kanji saved to customkanjilist');
  });

  router.post("/removekanji/:id", (req, res) => {
    var id = req.params.id;
    var kanji = req.body.kanji;
    User.findOneAndUpdate(
      { _id: id }, 
      { $pull: { customKanjiList: kanji } },
      function (error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log(success);
        }
     
      });
      return res.status(200).send('Kanji removed from customkanjilist');
  });
  module.exports = router;